<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Db_model extends CI_Model {
	function data_user(){
		return $this->db->get('login');
	}
	function get_id(){
	  $id = $this->session->userdata['']['id']; // dapatkan id user yg login
	  $this->db->select('id');
	  $this->db->where('id', $id);//
	  $this->db->from('login');
	  $query = $this->db->get();
	  return $query->result();
	}
}

/* End of file db_model.php */
/* Location: ./application/models/db_model.php */