<div class="jumbotron jumbotron1" style="background-color: #f1f5f9">
  <div id="teater" class="teater">
  
</div>
<div class="container jarak">
  <div class="col-md-4">
    <img src="<?=base_url()?>asset/img/kursi.png" class="img-responsive" width="300px;" height="auto">
  </div>
  <div class="col-md-8">
    <h2>Kursi Yang Nyaman</h2>
    <h5 align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
  </div>
</div>
<div class="container jarak"  id="teater">
  <div class="col-md-8">
    <h2>Kualitas Suara <b>No.1</b> di Indonesia </h2>
    <h5 align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
  </div>
  <div class="col-md-4">
   <img src="<?=base_url()?>asset/img/dolby.png" class="img-responsive" width="350px" height="auto"><br>
   <img src="<?=base_url()?>asset/img/dts.png" class="img-responsive" width="350px" height="auto">
  </div>
</div>
<div class="container"  id="teater">
  <div class="col-md-3">
    <img src="<?=base_url()?>asset/img/popcorn.png" class="img-responsive" width="200px" height="auto">
  </div>
  <div class="col-md-9"><br><br><br>
    <h2>Popcorn terbaik yang ada di Indonesia</h2>
    <h5 align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
  </div>
</div>
<div class="container jarak"  id="teater">
  <div class="col-md-8">
    <h2>Sensasi 4D yang lebih nyata dengan <b>4DX</b></h2>
    <h5 align="justify">Bagi kamu yang mencari pengalaman seru menonton, 4DX telah hadir di CGV Indonesia di 6 lokasi (Jakarta, Bandung, Yogyakarta, Surabaya). 4DX merupakan salah satu teknologi cinema yang sudah tersedia di 57 negara dan lebih dari 350 cinema di dunia. CGVian dapat menikmati serunya nonton di 4DX dengan konsep Absolute Cinema Experience, keseruan yang kamu rasakan dari 5 efek utama dari fitur 4DX (Gerakan, Angin, Air, Cahaya & Aroma), dimana lima indra perasa kamu akan benar-benar merasakan efeknya. Dapat kamu bayangkan serunya ketika adegan film kejar-kejaran dalam mobil atau motor, tegangnya adegan perkelahian, nuansa dingin ketika adegan romantis di malam hari, sampai aroma bunga yang akan tercium saat kamu menonton di 4DX. Teknologi 4DX menghadirkan sensasi baru saat menonton film, bahkan lebih dari simulator!
<br><br>
Teknologi 4DX dilengkapi dengan kursi yangdiprogram secara khusus dan sound system untuk mengikuti sesuai alur ceritadalam film yang sedang ditayangkan. Perangkat-perangkat khusus yang unik inimampu memberikan sensasi lebih pada penonton seolah-olah mereka dapat turutmerasakan emosi dan serunya cerita yang sedang dijalankan oleh para aktor didalam film tersebut. Pengalaman menonton film menjadi lebih realistis.
<br><br>Efek yang dihasilkan oleh 4DXadalah MOTION yaitu kursi yang bisa bergerak mengikuti alur cerita film, WINDyaitu efek angin yang membuat Anda merasakan ada di dalam film, SCENT yaitumembuat Anda bisa mencium aroma yang ada di cerita film, WATER yaitu efek airyang membuat Anda merasakan sensasi terciprat air, dan LIGHT yaitu efek kilatancahaya.</h5>
  </div>
  <div class="col-md-4">
   <img src="<?=base_url()?>asset/img/4dx.jpeg" class="img-responsive" width="350px" height="auto"><br>
   <img src="<?=base_url()?>asset/img/4dx2.gif" class="img-responsive" width="350px" height="auto">
  </div>

</div>
</div>
 
<div class="jumbotron2" style="background-color: #3a3f46">
  <div class="jumbo"></div><br>
  <div id="now-showing"></div><br><br><br>
<div class="container">
  <div class="col-md-12">
  <h2 class="warna-putih"><b>Upcoming Movie</b></h2>
  <h3 class="warna-putih"style="margin-top: 10px;">2018 Year</h3>
  <div class="row">
              <div class="col-xs-6 col-lg-2 container ea" style="position: relative;">   
                <img src="<?=base_url()?>asset/img/poster-doraemon.jpg" alt="..." class="image thumbnail">
                <div class="middle">
                 <h5><a href="">TRAILER <!-- : <?php echo $film[0]?> --></a></h5></div>
              </div>
              <div class="col-xs-6 col-lg-2 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-spiderman2.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                 <h5><a href="">TRAILER</a></h5></div>
                </div>
              <div class="col-xs-6 col-lg-2 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-thor.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                  <h5><a href="">TRAILER</a></h5></div>
              </div>
             <div class="col-xs-6 col-lg-2 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-daddy.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                  <h5><a href="">TRAILER</a></h5></div>
              </div></div>
  </div><br>
  <!-- <div class="col-md-6">
    <h2 style="margin-top: 0"class="warna-putih" align="right"><b>UP COMING MOVIE</b></h2>
    <h3 align="right" class="warna-putih"style=" margin-top: 10px;">Berlaku untuk bulan Oktober 2017</h3>
     <div class="row" align="right">
              <div class="col-xs-6 col-lg-4 container ea" style="position: relative;">   
                <img src="<?=base_url()?>asset/img/poster-doraemon.jpg" alt="..." class="image thumbnail">
                <div class="middle">
                 <h5><a href="">Trailer</a></h5></div>
              </div>
              <div class="col-xs-6 col-lg-4 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-spiderman2.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                 <h5><a href="">Trailer</a></h5></div>
                </div>
              <div class="col-xs-6 col-lg-4 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-thor.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                  <h5><a href="">Trailer</a></h5></div>
              </div>
             <div class="col-xs-6 col-lg-4 container ea">                
              
                <img src="<?=base_url()?>asset/img/poster-daddy.jpg" alt="..." class="image thumbnail">
                 <div class="middle">
                  <h5><a href="">Trailer</a></h5></div>
              </div></div>
  </div> -->
</div>
</div>
<script type="text/javascript">

     $(document).ready(function() {
       var navigasi = $(".carousel").offset().top;
       var sticky = function(){
         var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").addClass("aa");
         }
         else {
           $(".navbar-inverse").removeClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").removeClass("aa");
           }
         }
       sticky();
       $(window).scroll(function() {
         sticky();
       });
     });;;;
    $(document).ready(function() {
      var navigasi = $(".jumbotron1").offset().top;
      var sticky2 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").addClass("aaaa");
          $(".navbar-inverse .navbar-brand").addClass("aaaa");
        }
        else {
          $(".navbar-inverse").removeClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("aaaa");
          $(".navbar-inverse .navbar-brand").removeClass("aaaa");
        }
      }
      sticky2();
      $(window).scroll(function() {
        sticky2();
      });
    });;;;
$(document).ready(function() {
      var navigasi = $(".jumbo").offset().top;
      var sticky3 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").addClass("bbbb");
          $(".navbar-inverse .navbar-brand").addClass("bbbb");
        }
        else {
          $(".navbar-inverse").removeClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("bbbb");
          $(".navbar-inverse .navbar-brand").removeClass("bbbb");
        }
      }
      sticky3();
      $(window).scroll(function() {
        sticky3();
      });
    });
$('a[href^="#teater"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#now-showing"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#address"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#carousel-example-generic"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('.carosel-control-right').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').first().insertAfter($(this).parent().find('.carosel-item').last());
});
$('.carosel-control-left').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').last().insertBefore($(this).parent().find('.carosel-item').first());
});
</script>
