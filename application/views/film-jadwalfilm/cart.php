<?php
 if($this->session->userdata('status') == "login"){
  ?>
  <div class="jumbotron">
  <div class="container">
    <h2>SHOP CART <span class="glyphicon glyphicon-shopping-cart"></span></h2>
    <table class="table stripped">
      <thead><style type="text/css">th{text-transform: uppercase;}</style>
        <tr>
          <th>No</th>
          <th>Nama Pemesan</th>
          <th>FILM</th>
          <th>Jumlah Tiket</th>
          <th>Tanggal</th>
          <th>Studio</th>
          <th>Jam Film</th>
          <th>Harga Satuan</th>
          <th>Total Harga</th>
        </tr> 
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td><?=$this->session->userdata("nama");?></td>
          <td><?=$judulfilm;?></td>
          <td><?=$jmltiket;?></td>
          <td><?=$tgl?></td>
          <td><?php
          echo 'Studio ';
          echo $studio;
          ?>
          </td>
          <td><?=$jamfilm?></td>
          <td><?php
          echo "Rp. ";
          echo $harga?></td>
          <td><?php
          echo "Rp. ";
          echo $subtotal?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
  ><?php
  }
  else{
    echo'<script>
    location.href="http://localhost/lat_bioskop/"</script>';
  }
?>


<script type="text/javascript">

     $(document).ready(function() {
       var navigasi = $(".carousel").offset().top;
       var sticky = function(){
         var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").addClass("aa");
         }
         else {
           $(".navbar-inverse").removeClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").removeClass("aa");
           }
         }
       sticky();
       $(window).scroll(function() {
         sticky();
       });
     });;;;
    $(document).ready(function() {
      var navigasi = $(".jumbotron1").offset().top;
      var sticky2 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").addClass("aaaa");
          $(".navbar-inverse .navbar-brand").addClass("aaaa");
        }
        else {
          $(".navbar-inverse").removeClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("aaaa");
          $(".navbar-inverse .navbar-brand").removeClass("aaaa");
        }
      }
      sticky2();
      $(window).scroll(function() {
        sticky2();
      });
    });;;;
$(document).ready(function() {
      var navigasi = $(".jumbo").offset().top;
      var sticky3 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").addClass("bbbb");
          $(".navbar-inverse .navbar-brand").addClass("bbbb");
        }
        else {
          $(".navbar-inverse").removeClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("bbbb");
          $(".navbar-inverse .navbar-brand").removeClass("bbbb");
        }
      }
      sticky3();
      $(window).scroll(function() {
        sticky3();
      });
    });
$('a[href^="#teater"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#now-showing"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#address"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#carousel-example-generic"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('.carosel-control-right').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').first().insertAfter($(this).parent().find('.carosel-item').last());
});
$('.carosel-control-left').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').last().insertBefore($(this).parent().find('.carosel-item').first());
});
</script>
