<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$judul?></title>
    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/style.css">

    <script type="text/javascript" src="<?=base_url()?>asset/js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="<?=base_url()?>asset/js/bootstrap.js"></script>
</head>
<style>
.background{
	background-image: url(<?=base_url()?>asset/img/imx.jpg);
	width: 100%;
	height: 100%;
	
	position: absolute;
}
	.jarak{
		margin-top:80px;
	}
	.jarak-kecil{
		margin-top: 15px;
	}
.input-group .form-control{
	border:none;
	border-bottom: 2px solid#f7f7f7 ;
}
.input-group-addon:first-child {
    border-right: 0;
    border: none;
}
.panel-default{
box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.75);
animation: slideInDown 1s;
}
.google{
	padding: 8px;
	font-family: Tahoma;
	font-weight: bold;
	font-size:small;
	padding-left: 16px;
	padding-right: 16px;
	text-align: center;
}
.facebook{
	padding: 8px;
	font-family: Tahoma;
	font-weight: bold;
	font-size:small;
}
.google-icon{
	background-image: url(https://ecs7.tokopedia.net/img/icon/gplus_icon.png);
	    background-size: cover;
    width: 15px;
    height: 15px;
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 2px;
    margin-right: 5px;
}
.facebook-icon{
	background-image: url(https://ecs7.tokopedia.net/img/icon/gplus_icon.png);
	    background-size: cover;
    width: 15px;
    height: 15px;
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 2px;
    margin-right: 5px;
}
.panjang{
	width: 85px;
}
@keyframes slideInDown {
  from {
    transform: translate3d(0, -100%, 0);
    visibility: visible;
  }

  to {
    transform: translate3d(0, 0, 0);
  }
}
.jarak-mini{
	margin-top: 0px;
}
.slideInDown {
  animation-name: slideInDown;
}
</style>
<body>
	<div class="background" style="filter: brightness(90%) blur(2.5px);">
		&nbsp;
	</div>
	<!--<div class="col-md-4 col-md-offset-4 jarak">-->
<div class="col-md-4 col-md-offset-4 jarak">
		<div class="panel panel-default">
			<h2 align="center"><span class="glyphicon glyphicon-facetime-video"></span><b><a href="../<?php base_url()?>" style="color: black; text-decoration:none"> MOVIE</b>STAR</a></h2>
			  <div class="panel-body">
			  	<div class="col-md-12">
			 <form action="<?=base_url('register/cek_regist')?>" method="POST">
			    <div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
				  <input type="email" class="form-control" placeholder="Email" name="email">
				</div>
				<br>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <input type="text" class="form-control" placeholder="Username" name="username">
				</div>
				<br>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
				  <input type="password" class="form-control" placeholder="Password" name="password">
				</div><br>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
				  <input type="password" class="form-control" placeholder="Password Confirmation" name="password2">
				</div>
				<br>
				<div class="input-group">
					  <span class="input-group-addon"><span class=" glyphicon glyphicon-lock"></span></span>
				  <input type="text" class="form-control" placeholder="No HP" name="nohp">
				</div><br>
				<input style="float: left" value="Register" type="submit" name="register" class="btn btn-primary panjang" style="">
			</form>
				<a href=""><h5 style="float: right;">Forgot Password ?</h5></a><div class="jarak-kecil">&nbsp;</div>
				<h4 align="center">or</h4><div class="jarak-mini">&nbsp;</div>
				<button value=" type="submit" name="google" class="btn btn-block google "><span class="google-icon"></span>&nbsp;<a href="<?=base_url('login')?>">Masuk dengan Biasa</a> </button><div class="jarak-kecil"></div>
				<button value=" type="submit" name="google" class="btn btn-block google "><span class="google-icon"></span>&nbsp;Masuk dengan Google </button><div class="jarak-kecil"></div>
			  	<button value=" type="submit" name="google" class="btn btn-block google "><span class="google-icon"></span>&nbsp;Masuk dengan Facebook </button><div class="jarak-kecil"></div>
			 </div>
		</div>
	</div>
</body>
</html>
<style>
	.hr {
    background-color: #cdcdcd;
    height: 1px;
    width: 150px; //or inline-block
   	text-align: left;
    color: #cdcdcd;
	}
</style>