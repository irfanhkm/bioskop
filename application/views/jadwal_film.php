<div class="jumbotron">
  <div class="jumbo"></div><br>
    <div id="now-showing"></div><br><br><br>
  <div class="container"><br>
    <h2>Now Showing : 20 Januari 2018</h2>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#home" role="tab" data-toggle="tab">Jam : 12:00 | 14:15 | 16:30 | 18:45</a></li>
      <li><a href="#profile" role="tab" data-toggle="tab">Jam : 13:00 | 15:15 | 17:30 | 19:45</a></li>
<!--       <li><a href="#messages" role="tab" data-toggle="tab">Senin, 22 Januari 2018</a></li>
      <li><a href="#settings" role="tab" data-toggle="tab">Selasa, 23 Januari 2018</a></li> -->
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="home">
        <div class="row">
              <?php $this->load->view($film1)?>
              <!-- FILM2 -->
              <?php $this->load->view($film2)?>
            </div>
      </div>
      <div class="tab-pane" id="profile">
        <div class="row">
          <?php $this->load->view($film3)?>
          <?php $this->load->view($film4)?>

            </div>
      </div>
      <div class="tab-pane" id="messages">...</div>
      <div class="tab-pane" id="settings">...</div>
    </div>
  </div>
</div>

<script type="text/javascript">

     $(document).ready(function() {
       var navigasi = $(".carousel").offset().top;
       var sticky = function(){
         var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").addClass("aa");
         }
         else {
           $(".navbar-inverse").removeClass("fix");
           $(".navbar-inverse .navbar-nav > li > a").removeClass("aa");
           }
         }
       sticky();
       $(window).scroll(function() {
         sticky();
       });
     });;;;
    $(document).ready(function() {
      var navigasi = $(".jumbotron1").offset().top;
      var sticky2 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").addClass("aaaa");
          $(".navbar-inverse .navbar-brand").addClass("aaaa");
        }
        else {
          $(".navbar-inverse").removeClass("fix2");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("aaaa");
          $(".navbar-inverse .navbar-brand").removeClass("aaaa");
        }
      }
      sticky2();
      $(window).scroll(function() {
        sticky2();
      });
    });;;;
$(document).ready(function() {
      var navigasi = $(".jumbo").offset().top;
      var sticky3 = function(){
      var scrollTop = $(window).scrollTop();
        if(scrollTop > navigasi)
        {
          $(".navbar-inverse").addClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").addClass("bbbb");
          $(".navbar-inverse .navbar-brand").addClass("bbbb");
        }
        else {
          $(".navbar-inverse").removeClass("fix3");
          $(".navbar-inverse .navbar-nav > li > a").removeClass("bbbb");
          $(".navbar-inverse .navbar-brand").removeClass("bbbb");
        }
      }
      sticky3();
      $(window).scroll(function() {
        sticky3();
      });
    });
$('a[href^="#teater"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#now-showing"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#address"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('a[href^="#carousel-example-generic"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1500);
    }
});
$('.carosel-control-right').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').first().insertAfter($(this).parent().find('.carosel-item').last());
});
$('.carosel-control-left').click(function() {
  $(this).blur();
  $(this).parent().find('.carosel-item').last().insertBefore($(this).parent().find('.carosel-item').first());
});
</script>
