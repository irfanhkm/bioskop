<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

		function __construct(){
		  parent::__construct();
		  $this->load->model('login_model');
        }  
		 function index(){
            $data['judul']="Login Page";
          $this->load->view('login/login',$data);
		 }

		 function cek_login(){
		  $username = $this->input->post('username');
		  $password = $this->input->post('password');
		  $where = array(
		   'username' => $username,
		   'password' => md5($password)
		   );
		  $cek = $this->login_model->cek_login("login",$where)->num_rows();
		  if($cek > 0){

		   $data_session = array(
		    'nama' => $username,
		    'status' => "login"
		    );

		   $this->session->set_userdata($data_session);

		   redirect(base_url("hello"));

		  }else{
		   echo "<script type='text/javascript'>
		               alert ('Maaf Username Dan Password Anda Salah !');
                       location.href='http://localhost/lat_bioskop/login/';
		      </script>";
		  }
		 }

		 function logout(){
		  $this->session->sess_destroy();
		  echo "<script type='text/javascript'>
                alert ('Terimakasih., Anda sudah logout');
                location.href='http://localhost/lat_bioskop/login/';
          </script>";
        }
}