<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_model');
	}

	public function index()
	{
		$data['ss1']= $this->db_model->data_user()->result();
		$data['judul']="Landing Page";
		$data['konten']="home";
		$data['film'] = array("Spiderman 1","Spiderman 2");
		$this->load->view('template',$data);
	}
	public function login()
	{
		$data['judul']="Login Page";
		$data['konten']="login/login";
		$this->load->view('login/login',$data);
	}
	public function company()
	{
		$data['judul']="Company Profile";
		$data['konten']="company";
		$this->load->view('template',$data);
	}

	public function jadwal_film()
	{
		$data['judul']="Jadwal Film";
		$data['konten']="jadwal_film";
		$data['film1']="film-jadwalfilm/film1";
		$data['film2']="film-jadwalfilm/film2";
		$data['film3']="film-jadwalfilm/film3";
		$data['film4']="film-jadwalfilm/film4";
		$this->load->view('template',$data);
	}
	public function shopcart(){
		$this->load->view('home');
	}
	public function insertcart(){
		$data['judul']="SHOPCART";
		$data['konten']='film-jadwalfilm/cart';
		$data['ss1']= $this->db_model->data_user()->result();
		$data['harga'] = $this->input->post('harga');
		$data['jmltiket'] = $this->input->post('jmltiket');
		$data['tgl'] = $this->input->post('tgl');
		$data['studio'] = $this->input->post('studio');
		$data['jamfilm']= $this->input->post('jamfilm');
		$data['judulfilm']=$this->input->post('judulfilm');
		$data['subtotal']=$data['harga']*$data['jmltiket'];
		$this->load->model('insertdb');
		$this->insertdb->insertcart();
		$this->load->view('template', $data);
	}
		
}