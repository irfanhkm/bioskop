<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('insertdb');
    }  
	function index()
	{
		$data['judul']="Register Page";
		$data['konten']="register/register";
		$this->load->view('register/register',$data);
	}
	function cek_regist(){
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$password2=$this->input->post('password2');
		if ($password===$password2) {
		    if($this->insertdb->cek_regist()==$username){
		    	echo "<script type='text/javascript'>
			                alert ('Maaf Username sudah digunakan !');
	                        location.href='http://localhost/lat_bioskop/register/';
			      </script>";
            }
		    else{
		    	$this->load->model('insertdb');
				$this->insertdb->insertRegist();
				$this->load->view('login/login');
		    	
			} 
			
		}
		else{
			 echo "<script type='text/javascript'>
                alert ('Konfirmasi Password tidak sama');
                location.href='http://localhost/lat_bioskop/register/';
          </script>";
		}
		
	}

}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */